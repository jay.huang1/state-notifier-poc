
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:mocktail/mocktail.dart';
import 'package:state_notifier_poc/main.dart';

import 'unit_test.dart';

Future<void> main() async {
  testWidgets('Test MockPairNotifier ', (tester) async {
    final mockPairNotifier = MockPairNotifier();
    final pairData = Pair(parentValue: 2, childValue: 3);
    when(() => mockPairNotifier.increment()).thenAnswer((_) => '111 5 1');
    mockPairNotifier.debugSetState = pairData;

    await tester.pumpWidget(
      ProviderScope(
        overrides: [
          pairStateProvider.overrideWithValue(mockPairNotifier),
        ],
        child: MyApp(),
      ),
    );

    expect(find.text('2, 3'), findsOneWidget);
    await tester.tap(find.byType(ElevatedButton));
    await tester.pump();
    expect(find.text('3, 3'), findsNothing);

    verify(() => mockPairNotifier.increment()).called(1);
  });

  testWidgets('Test top level providers', (tester) async {
    final mockUserRepository = MockUserRepository();
    when(() => mockUserRepository.getUserId()).thenAnswer((_) => '111');

    await tester.pumpWidget(
      ProviderScope(
        overrides: [
          userRepositoryProvider.overrideWithValue(mockUserRepository),
          parentValueProvider.overrideWithProvider(StateProvider<int>((ref) => 2))
        ],
        child: MyApp(),
      ),
    );

    expect(find.text('2, 0'), findsOneWidget);
    await tester.tap(find.byType(ElevatedButton));
    await tester.pump();
    expect(find.text('2, 1'), findsOneWidget);

    verify(() => mockUserRepository.getUserId()).called(1);
  });
}