import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:mocktail/mocktail.dart';
import 'package:state_notifier_poc/main.dart';

class MockStateNotifier<T> extends StateNotifier<T> with Mock {
  MockStateNotifier(T state) : super(state);

  set debugSetState(T value) {
    state = value;
  }
}

class MockPairNotifier extends MockStateNotifier<Pair> implements PairNotifier {
  MockPairNotifier() : super(Pair(parentValue: 0, childValue: 0));
}

class MockUserRepository extends Mock implements UserRepository{}

void main() {
  final mockUserRepository = MockUserRepository();
  test('Test PairNotifier business logic.', () async {
    final container = ProviderContainer(overrides: [userRepositoryProvider.overrideWithValue(mockUserRepository)]);
    container.readProviderElement(parentValueProvider).setState(5);
    when(() => mockUserRepository.getUserId()).thenAnswer((_) => 'userId');

    final displayString = container.read(pairStateProvider.notifier).increment();
    final resultState = container.read(pairStateProvider);
    expect(
      displayString,
      'userId 5 1',
    );
    expect(
      resultState,
      Pair(parentValue: 5, childValue: 1),
    );
  });

  test('Test provider with mocked notifier.', () async {
    final mockCounter = MockPairNotifier();
    final pairData = Pair(parentValue: 2, childValue: 3);
    when(() => mockCounter.increment()).thenAnswer((_) => 'userId 5 1');
    mockCounter.debugSetState = pairData;

    final container = ProviderContainer(
      overrides: [pairStateProvider.overrideWithValue(mockCounter)],
    );
    final displayString = container.read(pairStateProvider.notifier).increment();
    expect(
      displayString,
      'userId 5 1',
    );
    final resultState = container.read(pairStateProvider);
    expect(
      resultState,
      pairData,
    );
  });
}