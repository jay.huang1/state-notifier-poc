import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class Pair with EquatableMixin {
  final int parentValue; //depends on parentValueProvider
  final int childValue;

  Pair({required this.parentValue, required this.childValue});

  Pair copyWith({int? parentValue, int? childValue}) {
    return Pair(parentValue: parentValue ?? this.parentValue, childValue: childValue ?? this.childValue);
  }

  @override
  String toString() {
    return 'parent: $parentValue, child: $childValue';
  }

  @override
  List<Object?> get props => [parentValue, childValue];
}

class UserRepository {
  String getUserId() => '123';
}

final parentValueProvider = StateProvider<int>((ref) => 0);
final userRepositoryProvider = Provider<UserRepository>((ref) => UserRepository());

class PairNotifier extends StateNotifier<Pair> {
  // Use ref to avoid rebuild when parent provider changed.
  PairNotifier(this.ref) : super(Pair(parentValue: ref.read(parentValueProvider), childValue: 0)) {
    // If part of the state has a dependency on top level provider
    ref.listen<int>(parentValueProvider, (previous, next) {
      state = state.copyWith(parentValue: next);
    });
  }

  final Ref ref;

  String increment() {
    final userRepo = ref.read(userRepositoryProvider);

    final second = state.childValue + 1;
    state = state.copyWith(childValue: second);
    final id = userRepo.getUserId();
    return "$id ${state.parentValue} ${state.childValue}";
  }

  void pairChanged(int? pre, int next) {}
}

final pairStateProvider = StateNotifierProvider<PairNotifier, Pair>((ref) {
  return PairNotifier(ref);
});

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends ConsumerStatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends ConsumerState<MyHomePage> {
  void _incrementCounter() {
    ref.read(parentValueProvider.notifier).state++;
  }

  @override
  Widget build(BuildContext context) {
    final pair = ref.watch(pairStateProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '${pair.parentValue}, ${pair.childValue}',
              style: Theme.of(context).textTheme.headline4,
            ),
            ElevatedButton(
              onPressed: () {
                ref.read(pairStateProvider.notifier).increment();
              },
              child: Text("Click"),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}